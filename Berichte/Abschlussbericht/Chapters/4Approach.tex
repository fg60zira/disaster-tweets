%*****************************************
\chapter{Methodik}\label{ch:approach}
%*****************************************
Dieses Kapitel beschäftigt sich mit unseren verwendeten Methoden zur Lösung des Problems.
Zunächst folgt die Exploration des Datensatzes, danach eine Beschreibung des Preprocessing und des Feature Engineering, sowie die Darlegung der verwendeten Algorithmen.
Wir unterscheiden dabei zwischen statistischen maschinellen Lernverfahren und neuronalen maschinellen Lernverfahren.
Da wir die Abarbeitung der eingangs vorgestellten Aufgaben als iterativen Prozess begreifen, liegt unser Fokus auf den statistischen maschinellen Lernverfahren.
Diese sind sowohl transparenter als auch leichtgewichtiger und somit schneller als neuronale Netze.
Da \citeauthor{8508520} (\citeyear{8508520}) mit einem neuronalen Netz jedoch die besten Ergebnisse erreicht haben, wollen wir diesen Ansatz nicht völlig ignorieren.

\section{Datensatz}

Dieser Abschnitt beschäftigt sich mit der Exploration der von Kaggle gelieferten Daten, 
stammend von der Firma \emph{Figure Eight}, einem in San Francisco ansässigem Tochterunternehmen von 
Appen.\footnote{\url{https://appen.com/}, aufgerufen am 22.02.2021} Sie unterteilen 
sich in einen Trainingsdatensatz und einen Testdatensatz, auf die im Nachfolgendem genauer eingegangen wird. 

Abbildung \ref{fig:train} zeigt einen Ausschnitt der gelieferten Daten aus Trainings- bzw. Testdatendatz.
Beide Datensätze enthalten die Columns \texttt{id}, \texttt{text}, \texttt{location} und \texttt{keyword}, während die Trainingsdaten zusätzlich eine Column
beinhalten, die das \texttt{target} beschreibt. 

\begin{itemize}
    \item \textbf{id}: identifiziert den Tweet
    \item \textbf{text}: Text des Tweets
    \item \textbf{location}: Ort, von dem der Tweet gesendet wurde 
    \item \textbf{keyword}: Keyword des Tweets (beschreibt zumeist die zugrundeliegende Katastrophe bzw. Stimmung)
    \item \textbf{target}: echte Katastrope (1) oder keine (0)
\end{itemize}



\begin{figure}[ht]
	\begin{minipage}[t]{.5\linewidth} % [b] => Ausrichtung an \caption
	   \includegraphics[width=\linewidth]{img/train_origin.png}
	\end{minipage}
	\hspace{.1\linewidth}% Abstand zwischen Bilder
	\begin{minipage}[t]{.5\linewidth} % [b] => Ausrichtung an \caption
	   \includegraphics[width=\linewidth]{img/test_origin.png}
	\end{minipage}
    \caption{Trainingsdatensatz (links) und Testdatensatz (rechts)}
    \label{fig:train}
 \end{figure}


Der Trainingsdatensatz beinhaltet insgesamt 7.613 Samples und weist 61 Fehlwerte (NaNs) 
bezüglich des Features \texttt{keyword} auf und 2.533 bezüglich \texttt{location}.
Betrachtet man nun die prozentuale Verteilung des Targets, so liegt diese bezüglich eines Disasters bei 43\%, bezüglich eines Non-Disasters bei 57\%
und ist somit nicht gleichverteilt, dennoch recht ausgeglichen.\\


Der Testdatensatz beinhaltet insgesamt 3.262 Samples, wobei 26 Fehlwerte in den Keywords und 
1.105 Fehlwerte bezüglich der Ortsangaben existieren.\\


Im Folgendem lohnt es sich nun, einen genaueren Blick auf die Features \texttt{keyword}, \texttt{location} und \texttt{text} zu werfen. Dazu wird zunächst 
die Frage geklärt, wieviele der in den Daten vorhandenen Werte bezüglich des Features einzigartig sind. 

\subsection{Einzigartigkeit der Feature-Werte}\label{ch:unique}

\begin{itemize}
    \item Wieviele der Keywords sind unique?
    \begin{itemize}
        \item Trainingsdatensatz: Von insgesamt 7.613 sind 222 Keywords unique
        \item Testdatensatz: Von insgesamt 3.263 sind 222 Keywords unique
    \end{itemize}
\end{itemize}

\begin{itemize}
    \item Wieviele der Ortsangaben sind unique?
    \begin{itemize}
      \item Trainingsdatensatz: Von insgesamt 7.613 sind 3.342 Locations unique
      \item Testdatensatz: Von insgesamt 3.263 sind 1.603 Locations unique
    \end{itemize}
\end{itemize}

\begin{itemize}
    \item Wieviele der Texte sind unique?
    \begin{itemize}
      \item Trainingsdatensatz: Von insgesamt 7.613 sind 7.503 der Tweet-Texte unique
      \item Testdatensatz: Von insgesamt 3.263 sind 32.43 der Tweet-Texte unique
    \end{itemize}
\end{itemize}



\subsection{Top-30-Verteilung der Features im Trainingsdatensatz}

Die im Anhang vorliegenden Abbildungen \ref{fig:top30keys0}, \ref{fig:top30keys1}, \ref{fig:loc0} und \ref{fig:loc1} bieten eine Übersicht über die häufig verwendeten Keywords, bzw.
Ortsangaben bezüglich Disaster und Non-Disaster Tweets. 
Hierbei wurden die vorhandenen Fehlwerte durch
die Werte \emph{missing\_keyword}, beziehungsweise \emph{missing\_location} ersetzt, um einen Eindruck zu erlangen, 
inwiefern das Feature für das Klassifikationsmodell von Bedeutung sein kann. \\

Die Qualität der Ortsangaben weist einige Mängel auf, so finden sich hier nicht nur verschiedene Schreibweisen
für dieselben Orte wieder, wie beispielsweise \emph{New York} und \emph{New York City}, sondern auch solche, die keinen konkreten Ort beschreiben, wie
zum Beispiel \emph{Worldwide}. Dies legt die Überlegung nahe, eine Standardisierung durchzuführen. Betrachtet man jedoch die 
Häufigkeit der fehlenden Werte, so stellt sich die Frage nach der Effektivität des Aufwandes der Standardisierung zum tatsächlichen 
Nutzen der Ortsangaben als Feature und wird daher im Zuge des Preprocessing zunächst keine Rolle spielen.\\ 


\subsection{Duplikate}
Im Zuge des Abschnitts \ref{ch:unique} zur Einzigartigkeit Tweet-Texte, fiel auf, dass die Datensätze Duplikate beinhalten.
Im Twitter Kontext können solche Duplikate als Retweets aufgefasst und als weiteres Feature verwendbar gemacht werden. Bei näherer 
Betrachtung der Duplikate kamen jedoch auch problematische Erkenntnisse zum Vorschein. Einige dieser Duplikate im Trainingsdatensatz sind unterschiedlich
gelabelt bezüglich ihre \texttt{targets}. Im Folgenden wird zunächst dennoch mit dem Mislabelling mancher Tweets gearbeitet und aus Zeitgründen
keine manuelle Anpassung durchgeführt. 

\section{Data Preprocessing}

Um die vorhandenen Daten für das maschinelle Lernen verwendbar zu machen, müssen alle Features, die nicht numerisch sind, in numerische Features umgewandelt werden. Dies betrifft alle Features unseres Datensatzes.
Zudem wollen wir aus dem Tweet-Text neue Features generieren, die wir für die Lösung des Klassifikationsproblems für geeignet halten.
Alle Transformationen führen wir auf dem Trainings- und dem Test-Datensatz durch.

\subsection{\texttt{keyword} und \texttt{location}}
\texttt{keyword} und \texttt{location} betrachten wir als kategoriale Features und kodieren die entsprechenden Klassen mit Integers.
Für die Transformation nutzen wir die Klasse \texttt{LabelEncoder} aus dem Paket \texttt{sklearn} \citep{scikit-learn}.

\subsection{Tweet-spezifisches Feature Engineering}
Bei Tweets handelt es sich um eine spezielle Textgattung mit Besonderheiten, die bei anderen Textarten nicht auftreten.
Solche Besonderheiten sind beispielsweise 
    \begin{itemize}
        \item \texttt{urls}: Verlinkungen auf externe Webseiten
        \item \texttt{mentions}: Erwähnung anderer Twitter-User
        \item \texttt{emojis}: Verwendung von Emojis im Text
        \item \texttt{hashtags}: Verwendung von Hashtags im Text 
        \item \texttt{retweets}: Posten des gleichen Tweet-Inhalts durch verschiedene User
    \end{itemize}

Diese Besonderheiten würden bei Standardverfahren zur Tokenisierung und Vektorisierung von Text verloren gehen, da beispielsweise URLs dort lediglich als Token geparst werden würden.
Dieser Token würde zudem auch nur wenige Male, wenn nicht sogar nur genau ein Mal im gesamten Korpus auftreten, sodass der Informationsgehalt für maschinelle Lernverfahren sehr gering ist. 
Wir vermuten, dass in den Informationen der tweet-spezifischen Besonderheiten jedoch wichtige Hinweise auf die Echtheit von Tweets stecken.
Auch \citeauthor{8709925} und \citeauthor{8508520} verwenden Hashtags und Retweets als Features für ihre Verfahren.
Die unterschiedliche Verteilung der Features unter den als wahr bzw. falsch gelabelten Daten im Trainingsdatensatz sützt diese These. 
Durch geeignetes Feature Engineering wollen wir diese Informationen als Features für das maschinelle Lernen verwendbar machen.
Dazu extrahieren wir in einem ersten Schritt die Features \texttt{urls}, \texttt{mentions} und \texttt{hashtags} aus dem Tweet-Text und wandeln sie in einem zweiten Schritt in eine Zahl um.
Wir entscheiden uns dafür bei allen Merkmalen für die Vorkommenshäufigkeit des Merkmals pro Tweet.
Für \texttt{emojis} fällt das Preprocessing weg, da es im Datensatz keine Emojis gibt. 
Tabelle \ref{tab:feature-stats} zeigt eine Übersicht der tweet-spezifischen Features und ihr Vorkommen im Trainingsdatensatz.

% Please add the following required packages to your document preamble:
% \usepackage{booktabs}
% \usepackage{graphicx}
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[ht]
    \centering
    \caption{Tweet-spezifische Features und ihr Vorkommen im Trainingsdatensatz.}
    \label{tab:feature-stats}
    \resizebox{\textwidth}{!}{%
    \begin{tabular}{@{}lrrrrr@{}}
    \toprule
    \textbf{}     & \multicolumn{1}{l}{\textbf{cnt\_emojis}}     & \multicolumn{1}{l}{\textbf{cnt\_mentions}} & \textbf{cnt\_hashtags}                     & \multicolumn{1}{l}{\textbf{cnt\_urls}}     & \multicolumn{1}{l}{\textbf{cnt\_retweets}} \\ \midrule
    \textbf{min}  & 0 & 0 & 0 & 0 & 0 \\
    \textbf{max}  & 0                                            & 8                                          & 13                                         & 4                                          & 23                                         \\
    \textbf{mean} & 0                                            & 0,356                                      & 0,438                                      & 0,620                                      & 0,567                                      \\ \bottomrule
    \end{tabular}%
    }
    \end{table}

\subsubsection{URLs}
URLs könnten dahingehend für die Klassifizierung als wahr oder fake dienen, dass wahre Tweets u.U. häufiger auf externe Quellen verweisen als falsche Tweets.
Im Trainingsdatensatz haben 66\% der wahren Tweets eine URL, aber nur 41\% der falschen Tweet. 
Wir entfernen URLs aus dem Tweet-Text und speichern sie zunächst als Liste in der eigenen Spalte \texttt{urls}.  
In der Spalte \texttt{cnt\_urls} wird die Länge dieser Liste, also die Anzahl der URLs im Tweet gespeichert. 

\subsubsection{Mentions}
Mentions auf Twitter sind Erwähnungen anderer User im Tweet-Text durch Verwendung des Zeichens \texttt{@}. 
Auch diese wollen wir aus dem Tweet-Text entfernen und sie in einer eigenen Spalte abspeichern.
Analog zum Vorgehen bei den URLs speichern wir die Anzahl der Mentions in der Spalte \texttt{cnt\_mentions}.
Auch hier könnte ein Zusammenhang zwischen Wahrheit der im Tweet beschriebenen Katstrophe und den Mentions bestehen. 
Beispielsweise wäre denkbar, dass offizielle Nachrichtenhäuser in ihren Berichten auf Mentions verzichten, während humorvoll gemeinte Katastrophenberichte mit Mentions der engen Freunde versehen sein könnten.
In den Trainingsdaten haben lediglich 20,3\% der wahren Tweets Mentions, aber 30,9\% der falschen Tweets.

\subsubsection{Hashtags}
Im Gegensatz zu URLs und Mentions wollen wir die Hashtags nicht vollständig aus dem Tweet-Text entfernen, da sie häufig grammatischer Bestandteil des Satzes sind und sogar häufig die wichtigsten Inhaltswörter der Aussage mit einem Hashtag versehen sind. 
Um diese Wörter später mit den Mitteln der NLP verarbeiten zu können, entfernen wir lediglich das \texttt{\#}-Symbol und speichern zudem die kompletten Hashtags als Liste in der Spalte \texttt{hashtags}, sowie deren Anzahl in der Spalte \texttt{cnt\_hashtags}.
26,2\% der wahren Tweets haben Hashtags und 20,4\% der falschen Tweets.

\subsubsection{Retweets}
Wie bereits erwähnt, sind nicht alle Tweets unique, sondern es befinden sich einige Duplikate im Datensatz.
Während 20,1\% der wahren Tweets im Trainingsdatensatz Duplikate sind, sind es nur 15,1\% der falschen Tweets.
Wir interpretieren diese Duplikate als Retweets.
Retweets könnten insofern Hinweis auf die Wahrhaftigkeit einer im Tweet beschriebenen Katastrophe dienen, dass Nutzer*innen durch das Retweeten einer Katastrophenmeldung ihre Follower*innen warnen wollen.
Für jeden Tweet im Datensatz ermitteln wir die Anzahl der Duplikate und speichern diese in der Spalte \texttt{cnt\_retweets} ab.

%TODO: Tabelle mit Features/PRozent im Datensatz wahr/falsch.
\subsection{Sentiment und Subjektivität}
Wir vermuten, dass vielen der als Nicht-Disaster klassifizierten Datenpunkte eine humorvolle oder ironisch gemeinte Bedeutung zugrunde liegt.
Wir wollen daher das Sentiment und die Subjektivität des Tweets als Features in unsere Trainingsdaten aufnehmen.
Dieses Vorgehen wurde auch von \citeauthor{8709925} und \citeauthor{8508520} verwendet. 
Für die Berechnung dieser Scores benutzen wir die Python-Libray \texttt{TextBlob} \citep{textblob}. 
Das Sentiment kann einen Wert im Intervall \texttt{[-1.0..1.0]} (negativ vs. positiv) annehmen.
Die Subjektivität beschreibt, wie subjektiv oder objektiv eine Äußerung ist und kann Werte im Intervall \texttt{[0.0..1.0]} annehmen. 
Zur Berechung dieser Werte benutzt TextBlob das Sentiment Lexicon SentiWordNet \citep{baccianella-etal-2010-sentiwordnet}. 
Dort sind Scores für \texttt{positivity}, \texttt{negativity} und \texttt{objectivity} für alle synsets in WordNet gespeichert. 
Die Sentiment- bzw. Subjektivitätsscores einer Phrase berechnet TextBlob aus dem Durchschnitt der Scores der Wörter, unter Berücksichtigung der Intensität vorhergehender Wörter (bspw. "very") und Negationen. 
Wir speichern die Scores in den Spalten \texttt{sentiment} und \texttt{subjectivity}. 
%TODO: wie TextBlob zu den Scores kommt: explained here: https://planspace.org/20150607-textblob_sentiment/

\subsection{Textvektorisierung}
Es gibt verschiedene Methoden, Text in numerische Features zu transformieren. 
Wir wollen folgende drei Methoden testen und die selben Machine-Learning Methoden mit den daraus resultierenden drei unterschiedlichen Datensätze trainieren:
\begin{enumerate}
    \item TF-IDF-Vektorisierung
    \item BOW-Vektorisierung
    \item WordEmbedding mit Word2Vec
\end{enumerate}

Auch \citeauthor{8508520} experimentieren mit verschiedenen Textvektoren (TF-IDF, Doc2Vec).

\subsubsection{TF-IDF-Vektorisierung}
Bei der TF-IDF-Vektorisierung wird eine Dokumentensammlung transformiert in eine Matrix der TF-IDF-Gewichte jedes Types in der Sammlung für jedes Dokument in der Sammlung.
Dazu nutzen wir die Klasse \texttt{TfidfVectorizer} aus scikit-learn \citep{scikit-learn}. 
Damit die Gewichte für den Trainings- und Testdatensatz gleich sind, werden die beiden Datensätze zuerst zusammengefügt, dann der Vectorizer gefittet, die Dokumente transformiert und die Datensätze danach wieder getrennt.
Da der TF-IDF-Vectorizer für jedes Dokument einen sparsen Vektor mit der Länge gleich der Anzahl der im Datensatz befindlichen Types erstellt, wächst unser Datensatz stark an. 
Die TF-IDF-Features umfassen 18.203 Spalten. 
Hinzu kommen die in den vorangehenden Abschnitten beschriebenen Features. 

\subsubsection{BOW-Vektorisierung}
Bei der BOW-Vektorisierung wird eine Dokumentensammlung in eine Matrix der Token Counts transformiert.
Dazu nutzen wir die Klasse \texttt{CountVectorizer} aus scikit-learn \citep{scikit-learn}. 
Die Vorgehensweise für die BOW-Transformation geschieht analog zur TF-IDF-Transformation.
Auch hier wächst der Datensatz um 18.203 Features an.

\subsection{Word Embeddings mit Word2Vec}
Word2Vec \citep{mikolov2013distributed} ist ein Sprachmodell, das Wörter eines Lexikons als Vektoren repräsentiert, so dass die Cosinus-Ähnlichkeit zwischen den Vektoren mit der semantischen Ähnlichkeit der repräsentierten Wörter korrespondiert.
Wir transformieren also den Datensatz in eine Matrix der Word2Vec-Vektoren. 
Dabei nehmen wir den Durchschnitt der Wort-Vektoren als Satz-Vektor.
Wir entscheiden uns für eine Vektor-Länge von 300, da diese Dimensionalität auch von den Autoren von Word2Vec bei verschiedenen Experimenten genutzt wird \citep{mikolov2013distributed}.
Folglich wächst der Datensatz nur um 300 Word2Vec-Features an.
Hinzu kommen wie bei den vorigen Modellen die zuvor beschriebenen Features.

\section{Maschinelles Lernen}
Es sollen verschiedene Verfahren des maschinellen Lernens trainiert werden, um zu klassifizieren, welche Tweets eine echte Katastrophe beschreiben und welche nicht.

Die finale Evaluation der Modelle soll dabei mit dem F1-Score geschehen.
Dieser wird von Kaggle für jede Submission der Vorhersage für den Test-Datensatz zurückgegeben.

\subsection{Statistische maschinelle Lernverfahren}
Wir vergleichen drei verschiedene statistische Klassifizierungsalgorithmen miteinander.
Dabei sollen alle Verfahren mit allen nachfolgend vorgestellten Featuresets überprüft werden, um nicht nur zu ermitteln, welcher Klassifikationsalgorithmus die besten Ergebnisse liefert, sondern auch welches Preprocessing am besten geeignet ist und welche Merkmale am meisten Vorhersagekraft besitzen.

Folgende Klassifikationsalgorithmen sollen getestet werden:
\begin{enumerate}
    \item Logistische Regression
    \item Naive Bayes
    \item Random Forest
\end{enumerate}
Es wurde zudem die Performance einer Support Vector Machine getestet.
Diese war jedoch so schlecht, dass dieser Klassifikator hier weiterhin keine Rolle spielen wird.

Die Hyperparameter der jeweiligen Modelle wurden getunt, um die best-möglichen Ergebnisse zu erzielen.
Dazu nutzen wir die Klasse \texttt{GridSearchCV} aus dem Paket scikit-learn \citep{scikit-learn}.
Damit kann die optimale Kombination aus Werten für verschiedene Hyperparameter ermittelt werden.
Das Parameter-Tuning erfolgt bei jedem Klassifikator individuell für jeden Textvektor.
Die Ergebnisse der Klassifikatoren vergleichen wir zusätzlich mit einem naiven Dummy-Classifier, der für jeden Datenpunkt \texttt{fake} vorhersagt.
Für jeden Datensatz sollen die verschiedenen Klassifikatoren die Klassen im Test-Datensatz vorhersagen. 

% irgendwo erwähnen dass wir im Vergleich zum related work sehr wenig Featrue zur Verfügung haben


Weiterhin wollen wir den Einfluss der veschiedenen Features auf die Performance untersuchen.
Wir definieren folgende Feature-Sets:
\begin{itemize}
    \item Textvektor: alle Features die duch Textvektorisierung entstanden sind
    \item Counts: \texttt{cnt\_emojis}, \texttt{cnt\_mentions}, \texttt{cnt\_urls}
    \item Kategoriale Features: \texttt{location}, \texttt{keyword}
    \item Sentiment: \texttt{sentiment}, \texttt{subjectivity}
    \item Retweets: \texttt{cnt\_retweets}
\end{itemize}

Für den Vergleich der Performance testen wir folgende Kombinationen an Feature-Sets:
\begin{itemize}
    \item Counts + Sentiment
    \item Counts + Sentiment + kat. Features
    \item nur Textvektor
    \item Textvektor + Counts
    \item Textvektor + Counts + Retweets
    \item Textvektor + Counts + Retweets + kat. Features
    \item alle Features
\end{itemize}

Da die Anzahl der Submissions bei Kaggle auf 5 pro Tag begrenzt ist, teilen wir den Trainingsdatensatz in Train und Test (80/20-Split), damit wir die Performances der verschiedenen Konfigurationen miteinander vergleichen können.
Als Evaluationsmaße dienen uns Precision, Recall, Accuracy und F1-Score.
Diese berechnen wir mittels der jeweiligen Klassen in scikit-learn.
Die beste Prediction pro Klassifikator wird dann bei Kaggle hochgeladen, um den Score zu erhalten.

\subsubsection{Logistische Regression}
Zur Implementierung der Logistischen Regression nutzen wir scikit-learn's \texttt{LogisticRegression} \citep{scikit-learn}.
Als Solver wurde \texttt{liblinear} gewählt, da die Regression mit anderen Solvern, bspw. \texttt{lbfgs} nicht konvergierte.
Es wurden die Parameter \texttt{C} und \texttt{penalty} getunt.

\subsubsection{Bernoulli Naive Bayes}
Bernoulli Naive Bayes ist eine Naive Bayes Methode, die insbesondere dafür geeignet ist, Vorhersagen für Bool'sche Variablen zu treffen.
Zur Implementierung nutzen wir scikit-learn's \texttt{BernoulliNB} \citep{scikit-learn}.
Es wurde der Parameter \texttt{alpha} getunt. Dabei handelt es sich um den Smoothing-Parameter des Naive-Bayes-Algorithmus.

\subsubsection{Random Forest}
Zur Implementierung des Random Forest nutzen wir scikit-learn's \texttt{RandomForestClassifier} \citep{scikit-learn}.
Hierbei wurde der Parameter \texttt{n\_estimators} getunt. Dieser bestimmt die Anzahl der Bäume im Wald. 

\subsection{Neoronale maschinelle Lernverfahren - BERT}
Auch wenn unser Hauptaugenmerk auch auf den transparenteren statistischen maschinellen Lernverfahren liegt, wollten wir dennoch einen state-of-the-art NLP-Ansatz zur Lösung des Problems anwenden.

Das von Google entwickelte Deep-Learning-Modell BERT steht für \emph{Bidirectional Encoder Representation from Transformers} 
und erweist sich nach \citeauthor{Gao:2019} (\citeyear{Gao:2019}) bei einer Vielzahl von Aufgaben zur Verarbeitung natürlicher Sprache als äußerst erfolgreich. 
Nach \citeauthor{Devlin:2018} (\citeyear{Devlin:2018}) können tiefe bidirektionale Repräsentationen aus unmarkiertem Text trainiert werden, sodass als Ergebnis ein vortrainiertes BERT-Modell 
entsteht. 
Für dieses benötigt der Anwender nur ein aufgabenspezifisches Feintuning, ohne wesentliche Architekturänderungen vorzunehmen.

Devlin und Chang beschreiben in ihrem Blogpost, dass eine der größten Herausforderungem im Bereich des Natural Language Processings im Fehlen 
vieler Trainingsdaten besteht.\footnote{\url{https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html}, aufgerufen am 23.02.2021}
Vortrainiert auf Texten aus Wikipedia und dem Google-Books-Corpus, ermöglicht BERT, mit einem recht kleinen Datensatz dennoch gute Ergebnisse zu erzielen. 

Somit erweist sich die Nutzung eines der frei zugänglichen vortrainierten BERT-Modelle zur Klassifikation der englischprachigen Tweets als geeignetes Mittel. 

Das im Zuge dieses Projekts verwendete Base-Modell uncased, wurde auf
englischprachigem, kleingeschriebenem Text vortrainiert und verwendet 12 \emph{layer}, beziehungsweise Transformerblöcke, 
12 \emph{attetion heads}, 768 \emph{hidden nodes} und 110 Millionen Parameter. \footnote{\url{https://huggingface.co/transformers/pretrained_models.html}, aufgerufen am 23.02.2021}
Über TensorFlow Hub kann dieses gedownloadet werden.\footnote{\url{https://tfhub.dev/tensorflow/bert_en_uncased_L-24_H-1024_A-16/1}, aufgerufen am 23.02.2021}

Das vortrainierte BERT-Modell dient als Input-Layer für unser neuroanles Netz.
Dieses besteht ausschließlich aus Dense Layers, also keinen zusätzlichen Convolutional Layers.
Auf diese mussten wir aufgrund mangelnder Rechenkapazität verzichten.
Das Training wurde in zwei bzw. 10 Epochen ausgeführt.
Als Datensatz wird der aus dem Preprocessing entstandene Base Datensatz (s. Abbildung \ref{fig:base}), verwendet mit zusätzlichem lowercasing, dem Modell entsprechend.
