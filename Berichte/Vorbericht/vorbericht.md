# Disaster Tweets


#### Gruppenmitglieder: 
* Phil-Alexander Hofmann (3705462)
* Miriam Amin (3750251)
* Franziska Görg (3746117)

## Gegenstand, Problematik, Motivation
Twitter, Facebook und Co. als soziale Medien bieten die Grundlage zur rapiden Verbreitung von Nachrichten. Hierbei können Beiträge, wie Tweets in Echtzeit eingesehen und gerepostet werden. Unter diesen erscheinen jedoch auch solche, die sich als Fake News herausstellen. Beispielsweise wurden im Zuge des Hurricane Irma 2017,  nach Johnson [1], falsche Informationen verbreitet, dass eben dieser Houston erreichen würde, ohne dass fundierte Wettervorhersagen vorliegen. So bewirken Falschmeldungen im katastrophalen Kontext große Verwirrung und Unsicherheit. 
Unter diesen News erscheinen jedoch auch solche, die auf metaphorische Art und Weise ein Geschehen beschreiben, dem positive Gefühle unterliegen und es sich nicht um eine klassische Katastrophe handelt. Insbesondere durch die Unerstützung des Tweets mittels Bildmaterial, ist dem menschlichen Auge der Zusammenhang klar, ob es sich um eine tatsächliche Katastrophe oder um ein aussergewöhnlich schönes Erlebnis, gestütz durch katasrophal starke Worte, handelt.

Abbildung 1 zeigt beispielsweise einen Tweet von dem aktuell wütenden Hurricane Iota. Vorstellbar wäre jedoch auch, dass ein begeisterter Surfer von monströsen Wellen zu sprechen vermag, bei zugrundeliegender positiver Stimmung ohne katastrophalem Zusammenhang. 

<figure>
<img src="iota.jpg" alt="iota.jpg" width="400"/>
<font size="2">
<figcaption> Abbildung 1: Ioata Hurricane - Tweet
</figcaption>
</font>
</figure>


Die erwähnte schnelle Verbreitung von Neuigkeiten über soziale Medien birgt jedoch im Katastrophenkontext großes Potential. Handelt es sich um tatsächliche Katastrophen, gilt das Motto der Verbreitung "je schneller, desto besser". Daher ist es auch für Nachrichtenagenturen von Interesse, vorhersagen zu können, ob ein Tweet eine echte Katastrophe beschreibt. 

Mittels der von Kaggle veröffentlichen Challenge [*Real or Not? NLP with Disaster Tweets*](https://www.kaggle.com/c/nlp-getting-started), wird Entwicklern ein Datensatz geboten, um sich mit der Problematik auseinanderzusetzten und sich der Lösung des Probelms mittels Verfahren aus dem Bereich des Natural Language Processings zu nähern. 



## Zielsetzung
Ziel dieses Projekts ist es, ein Machine Learning Modell zu erstellen, welches durch binäre Klassifikation vorhersagen kann, ob ein Tweet eine reale oder keine reale Katastrophe beschreibt. 


## Aufgaben
Zur Organisation des Projektes, der Aufgaben und letztendlich der Entwicklung des Modells zur Klassifikation wird [Gitlab](https://git.informatik.uni-leipzig.de/fg60zira/disaster-tweets) verwendet. Um das Ziel zu erreichen gilt es, die vorliegenden Daten zu explorieren, zu reinigen, Ideen für geeignete ML-Verfahren zu sammeln und eben diese anzuwenden. Zur Beschreibung geeigneter Aufgaben lohnt es sich zunächst einen ersten Eindruck über die vorliegenden Daten zu gewinnen, um mögliche Lösungsschritte formulieren zu können. 

### Daten
Der verwendete Datensatz stammt von der Firma [*Figure Eight*](https://en.wikipedia.org/wiki/Figure_Eight_Inc.) und wurde erstmals auf ihrer [*Data for everyone Website*](https://appen.com/resources/datasets/) veröffentlicht. 

Der Datensatz beinhaltet Tweets, die tatsächliche Katastrophen beschreiben, und solche, die dies nicht tun. 

#### Features 

Der Trainingsdatensatz hat insgesamt 7613 Samples und 4 Features.

**keyword:** Ein Keyword aus dem Tweet. Weist viele Fehlwerte auf  
**location:** Der Ort von dem der Tweet aus abgesetzt wurde. Weist ebenfalls viele Fehlstellen auf  
**text:** Der Text des Tweets  
**target:** Das Ziel-Feature. 0 für *not a disaster*  und 1 *disaster*

Der Testdatensatz hat 3263 Samples.

Alle Features liegen als String vor.

#### Fehlwerte

Test und Trainingsdatensatz weisen in den Features die folgende Anzahl an Fehlwerten auf:

 Feature | Training | Test
 --- | --- | --- 
 keyword | 61 | 26
 location | 2.533 | 1.105
 text | 0 | 0

#### Feature Engineering

Um die Daten für Methoden des Maschinellen Lernens verwendbar zu machen, müssen sie in numerische Features überführt werden. 

**Mögliche Features wären dabei**
* Vorhandensein von Ortsangaben im Tweet/Anzahl
* Anzahl/Vorhandensein Rechtschreibfehler
* Anzahl/Vorhandensein Emojis
* Sentiment
* Katastrophen-Keywords
* Wortfrequenzen
* Word Embeddings


#### Preprocessing

Um den Text demensprechend verarbeiten zu können, muss er vorverarbeitet werden. Dafür sind u.a. folgende Schritte relevant
* lowercasing
* tokenization
* stop word removal
* emoji removal
* punctuation removal
* typo correction
* stemming and lemmatization




## Aufgabenverteilung

Das Team wird Aufgaben mittels GitLab Issues dokumentieren und verteilen. Die Zuordnung findet dabei unter Absprache untereinander ab. Dabei wird darauf geachtet, dass die Arbeitsverteilung innerhalb des Teams ausgeglichen ist. Es sind wöchentliche Meetings geplant, die im [Wiki](https://git.informatik.uni-leipzig.de/fg60zira/disaster-tweets/-/wikis/home) unter dem Eintrag *Kalender* dokumentiert werden. 

## Zeitplan 
Ein grober Zeitplan kann im Gitlab des Projektes eingesehen werden, durch die Verwendung von [Milestones](https://git.informatik.uni-leipzig.de/fg60zira/disaster-tweets/-/milestones). Während die einzelnen Zeitfenster der Milestones bezüglich der Entwicklungsschritte des Projekts jedoch zunächst eher als Orientierung anzusehen sind und sich im Verlaufe des Projektes gegebenenfalls anpassen können, so ist als Ende des Projektes Mitte Januar 2021 festgelegt, sowie die Fertigstellung des Endberichts Mitte Februar 2021. 



## Quellen
[1] Bridget Johnson, "Fake News During Disasters Putting People’s Lives at Risk, Warns Intel Bulletin",  Homeland Security Today, 2020 -> [Zugriff via Homeland Security Today, aufgerufen am 18.11.2020](https://www.hstoday.us/subject-matter-areas/emergency-preparedness/fake-news-during-disasters-putting-peoples-lives-at-risk-warns-intel-bulletin/)